﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappling_hook : MonoBehaviour
{
    [SerializeField]
    private float KickSpeed;
    RaycastHit hit;
    [SerializeField]
    private AudioClip Pull;
    [SerializeField]
    private AudioClip Push;
    [SerializeField]
    private ParticleSystem Sparks;
    private AudioSource source;
    private void Start()
    {
        source = GetComponent<AudioSource>();
        Sparks.Stop();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
            if (Physics.Raycast(ray, out hit, 10f, -1))
                if (hit.collider != null)
                {
                    GetComponent<SpringJoint>().connectedBody = hit.rigidbody;
                    hit.rigidbody.useGravity = false;
                    source.PlayOneShot(Pull, 0.5f);
                    Sparks.Play();
                    Sparks.transform.position = hit.transform.position;
                }
        }
        if (Input.GetMouseButtonUp(1) || Input.GetMouseButtonDown(0))
        {
            GetComponent<SpringJoint>().connectedBody = null;
            if (hit.collider != null)
                hit.rigidbody.useGravity = true;
            Sparks.Stop();
        }
        if (Input.GetMouseButton(1) && Input.GetMouseButtonDown(0))
        {
            Sparks.Stop();
            hit.rigidbody.velocity = this.transform.parent.forward * KickSpeed;
            source.PlayOneShot(Push, 0.5f);
        }
        Sparks.transform.position = hit.transform.position;
    }
}