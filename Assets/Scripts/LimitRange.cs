﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitRange : MonoBehaviour
{
    [SerializeField]
    private Transform source;
    [SerializeField]
    private float radius;
    [SerializeField]
    private float scale;
    private void FixedUpdate()
    {
        Vector3 offset = gameObject.transform.position - source.transform.position;
        gameObject.transform.position = Vector3.Scale(gameObject.transform.position, new Vector3(scale, scale, scale)) + Vector3.Scale(source.transform.position + Vector3.ClampMagnitude(offset, radius), new Vector3(1f - scale, 1f - scale, 1f - scale));
    }
}
