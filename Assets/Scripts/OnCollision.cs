﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollision : MonoBehaviour {
    public int RespawnTime;
    void OnCollisionEnter(Collision col)
    {
        float resilience = Random.Range(0, 10);
        float damage = col.relativeVelocity.magnitude - resilience;
        if (damage > 5)
        {
            GameObject.FindWithTag("Kills").GetComponent<UpdateUI>().AddKill();
            this.transform.parent.parent.parent.parent.parent.parent.parent.GetComponent<Animator>().enabled = false;
            StartCoroutine("Respawn");
        };
    }
    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(RespawnTime);
        this.transform.parent.parent.parent.parent.parent.parent.parent.GetComponent<Animator>().enabled = true;
    }
}