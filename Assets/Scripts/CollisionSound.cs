﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSound : MonoBehaviour
{
    [SerializeField]
    private AudioClip crashSoft;
    [SerializeField]
    private AudioClip crashHard;

    private AudioSource source;
    private float lowPitchRange = .8F;
    private float highPitchRange = 1.2F;
    [SerializeField]
    private float velToVol;
    private float velocityClipSplit = 10F;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }
    void OnCollisionEnter(Collision coll){
        source.pitch = Random.Range(lowPitchRange, highPitchRange);
        float hitVol = coll.relativeVelocity.magnitude * velToVol;
        if (coll.relativeVelocity.magnitude < velocityClipSplit)
            source.PlayOneShot(crashSoft, hitVol);
        else
            source.PlayOneShot(crashHard, hitVol);
    }
}