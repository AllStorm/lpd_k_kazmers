﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class AgentUpdate : MonoBehaviour {
    public Transform target;
    private Animator anim;
    public ThirdPersonCharacter character { get; private set; }
    NavMeshAgent agent;
	void Start () {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        character = GetComponent<ThirdPersonCharacter>();
        agent.updateRotation = false;
        agent.updatePosition = true;
    }

    private void Update()
    {
        if (target != null)
            agent.SetDestination(target.position);

        if (agent.remainingDistance > agent.stoppingDistance)
            character.Move(agent.desiredVelocity, false, false);
        else
            character.Move(Vector3.zero, false, false);
    }
}
