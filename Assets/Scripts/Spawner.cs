﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    [SerializeField]
    private GameObject Zombie;
    [SerializeField]
    public GameObject[] Zombies;
    [SerializeField]
    private int ZombieCount;
    private Rigidbody gameObjectsRigidBody;
    private Vector3 pos;
    private Quaternion rot;
    void Start()
    {
        Zombies = new GameObject[ZombieCount];
        pos = Zombie.transform.position;
        rot = Zombie.transform.rotation;
        for (int i = 0; i < Zombies.Length; i++)
        {
            Spawn(i);
        }

    }
    void Spawn(int i)
    {
        pos.x += Random.Range(-2, 2);
        pos.z += Random.Range(-2, 2);
        GameObject ZombieObject = (GameObject)Instantiate(Zombie, pos, rot);
        ZombieObject.transform.Rotate(Vector3.up, Random.Range(0, 360));
        ZombieObject.transform.parent = gameObject.transform;
        Zombies[i] = ZombieObject;
    }
}
