﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hammer_Collision : MonoBehaviour
{
    [SerializeField]
    private AudioClip crashSoft;
    [SerializeField]
    private AudioClip crashHard;
    [SerializeField]
    private AudioClip FleshHit;


    private AudioSource source;
    private float lowPitchRange = .8F;
    private float highPitchRange = 1.2F;
    [SerializeField]
    private ParticleSystem system;
    [SerializeField]
    private float velToVol;
    private float velocityClipSplit = 10F;


    void Awake()
    {
        system.Stop();
        source = GetComponent<AudioSource>();
    }


    void OnCollisionEnter(Collision coll)
    {
        if (coll.transform.tag == "Zombie")
        {
            if (coll.relativeVelocity.magnitude < velocityClipSplit)
                source.PlayOneShot(FleshHit, 0.1f);
            else
                source.PlayOneShot(FleshHit, 0.5f);
            StartCoroutine("Splat");
        }
        else
        {
            source.pitch = Random.Range(lowPitchRange, highPitchRange);
            float hitVol = coll.relativeVelocity.magnitude * velToVol;
            if (coll.relativeVelocity.magnitude < velocityClipSplit)
                source.PlayOneShot(crashSoft, hitVol);
            else
                source.PlayOneShot(crashHard, hitVol);
        }
    }
    IEnumerator Splat()
    {
        system.Play(true);
        yield return new WaitForSeconds(0.1f);
        system.Stop();
    }
}