﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartEvent : MonoBehaviour
{
    [SerializeField]
    private GameObject myGameObject;
    [SerializeField]
    private GameObject[] Cubes;
    private Rigidbody gameObjectsRigidBody;
    private Vector3 pos;
    private Vector3 ObjectPos;
    private Quaternion rot;
    void Start()
    {
        Cubes = new GameObject[200];
        ObjectPos = myGameObject.transform.position;
        rot = myGameObject.transform.rotation;
        for (int i = 0; i < Cubes.Length; i++)
        {
            pos = ObjectPos;
            pos.z += Mathf.Sin(Mathf.PI / 30 * (i * 20.5f)) * 3;
            pos.x += Mathf.Cos(Mathf.PI / 30 * (i * 20.5f)) * 3;
            pos.y = 1.01f * i;
            GameObject Cube = (GameObject)Instantiate(myGameObject, pos, rot);
            Cube.transform.Rotate(Vector3.up, i*5);
            Cube.transform.parent = gameObject.transform;
            Cube.GetComponent<Rigidbody>().Sleep();
            Cubes[i] = Cube;
        }
    }

}