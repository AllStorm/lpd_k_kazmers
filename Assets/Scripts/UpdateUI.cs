﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateUI : MonoBehaviour {
    public static int kills;
    Text text;
	void Update () {
        text = GetComponent<Text>();
        text.text = "Kills: " + kills;
	}
    public void AddKill(){
        kills++;
    }
}
