﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{

    [SerializeField]
    private Transform player;

    private Animator anim;
    private float dist;
    private GameObject[] Zombies;

    private void Start()
    {
        anim = GetComponent<Animator>();
        Zombies = GameObject.Find("Zombies").GetComponent<Spawner>().Zombies;
    }

    void Update()
    {
        dist = Vector3.Distance(player.position, this.transform.position);
        if ((dist < 10 && dist > 2) && anim.enabled)
        {
            anim.SetBool("Running", true);
            anim.SetBool("Walking", false);
            Vector3 direction = player.position - this.transform.position;
            direction.y = 0;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);
            foreach (GameObject zombie in Zombies)
            {
                if ((Vector3.Distance(zombie.transform.position, this.transform.position) < 2) && this.gameObject != zombie && zombie.GetComponent<Animator>().enabled)
                {
                    direction = this.transform.position - zombie.transform.position;
                    direction.y = 0;
                    this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);
                }
            }
        }
        else if(anim.enabled)
        {
            anim.SetBool("Running", false);
            anim.SetBool("Walking", true);
            Vector3 direction = player.position - this.transform.position;
            direction.y = 0;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);
            foreach (GameObject zombie in Zombies)
            {
                if ((Vector3.Distance(zombie.transform.position, this.transform.position) < 2) && this.gameObject != zombie && zombie.GetComponent<Animator>().enabled)
                {
                    direction = this.transform.position - zombie.transform.position;
                    direction.y = 0;
                    this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);
                }
            }
        }
        else
        {
            anim.SetBool("Running", false);
            anim.SetBool("Walking", false);
        }
    }
}